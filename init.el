(global-set-key (kbd "C-z") 'undo )

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                           ("gnu" . "https://elpa.gnu.org/packages/")
                           ("nongnu" . "https://elpa.nongnu.org/nongnu/")))

;;("org" . "https://orgmode.org/elpa/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(setq user-full-name "Oscar")
(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room

(menu-bar-mode 1)          ; Disable the menu bar



(column-number-mode)
(global-display-line-numbers-mode t)

;; sustituye yes/no por y/n
(fset 'yes-or-no-p 'y-or-n-p)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
		treemacs-mode-hook
		eshell-mode-hook
		wallabag-search-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Dislay Clock
(display-time)

(setq
 display-time-24hr-format t           ; Muestra el reloj en formato 24 hrs
 display-time-format "%H:%M"          ; Le da formato a la hora
 load-prefer-newer t                  ; Prefiere la versión más reciente de un archivo.
 select-enable-clipboard t            ; Sistema de fusión y portapapeles de Emacs.
 vc-follow-symlinks t                 ; Siempre sigue los enlaces simbólicos.
 make-backup-files nil                ; No realiza backups de ficheros
 auto-save-default nil                ; Deshabilita #file#
 ;; org-footnote-section "Referencias:"  ; cambio footnotes por referencias
 ;; global-hl-line-mode t                ; Highlight current line
 kill-ring-max 128                    ; Longitud máxima del anillo de matar
 create-lockfiles nil                 ; Impido la creación de ficheros .#
 )

(set-face-attribute 'default nil :font "JetBrains Mono NerdFont" :height 100)

;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "JetBrains Mono NerdFont" :height 100)

;; ESC para salr de los prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Incrementa el tamaño de la fuente
(global-set-key (kbd "C-+") 'text-scale-increase)

;; Disminuye el tamaño de la fuente
(global-set-key (kbd "C--") 'text-scale-decrease)

;; Mover entre ventanas con teclas dirección
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

(use-package uwu-theme
:ensure t
:config
(load-theme 'uwu t))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1))

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("C-M-b" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1))

(use-package perspective
  :bind
  ("C-x C-b" . persp-list-buffers)
  ("C-x k" . persp-kill-buffer*)
  :custom
  (persp-mode-prefix-key (kbd "C-c C-p"))
  :init
  (persp-mode -1))

;(setq auth-sources '("~/.gnupg/shared/authinfo.pgp"))

(use-package auto-package-update
  :custom
  (auto-package-update-interval 7)
  (auto-package-update-delete-old-versions t)
  (auto-package-update-prompt-before-update t)
  (auto-package-update-hide-results t)
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "09:00"))

(setq ispell-program-name "aspell")                                 (setq ispell-local-dictionary "espanol")
(setq ispell-local-dictionary-alist
      '(("espanol" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil nil nil utf-8)))

(use-package flyspell
  :defer 1
  :delight
  :custom
  (flyspell-abbrev-p t)
  (flyspell-issue-message-flag nil)
  (flyspell-issue-welcome-flag nil)
  (flyspell-mode 1))

(use-package flyspell-correct-ivy
  :after flyspell
  :bind (:map flyspell-mode-map
              ("C-;" . flyspell-correct-word-generic))
  :custom (flyspell-correct-interface 'flyspell-correct-ivy))

;; Activar Flyspell en modo texto y modos relacionados
(add-hook 'text-mode-hook
          #'(lambda () (flyspell-mode 1)))

(defun fd-switch-dictionary()
  (interactive)
  (let* ((dic ispell-current-dictionary)
         (change (if (string= dic "espanol") "english" "espanol")))
    (ispell-change-dictionary change)
    (message "Dicionario cambiado desde %s a %s" dic change)
    ))

(global-set-key (kbd "<f6>")   'fd-switch-dictionary)

(use-package whitespace
  :ensure
  :defer 1
  :hook (before-save . delete-trailing-whitespace))

(use-package hungry-delete
  :ensure t
  :defer 0.7
  :delight
  :config (global-hungry-delete-mode))

(use-package autorevert
  :ensure nil
  :diminish
  :hook (after-init . global-auto-revert-mode))

(global-set-key [remap kill-buffer] #'kill-this-buffer)
  (kill-buffer "*scratch*")

(require 'dashboard)
(dashboard-setup-startup-hook)
;; Or if you use use-package
;; Set the title
(setq dashboard-banner-logo-title "Welcome to Emacs a true free editor")
;; Set the banner
(setq dashboard-startup-banner  'logo )
;; Value can be
;; - nil to display no banner
;; - 'official which displays the official emacs logo
;; - 'logo which displays an alternative emacs logo
;; - 1, 2 or 3 which displays one of the text banners
;; - "path/to/your/image.gif", "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever gif/image/text you would prefer
;; - a cons of '("path/to/your/image.png" . "path/to/your/text.txt")

;; Content is not centered by default. To center, set
(setq dashboard-center-content t)

;; To disable shortcut "jump" indicators for each section, set
(setq dashboard-show-shortcuts nil)

(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (agenda . 5)))
(setq dashboard-set-heading-icons t)
(setq dashboard-set-file-icons t)
()
(when (display-graphic-p)
  (require 'all-the-icons))

  ;; F10 para ir al Dashboard
    (global-set-key (kbd "<f10>") 'open-dashboard) ;

(defun open-dashboard ()
  "Open the *dashboard* buffer and jump to the first widget."
  (interactive)
  (delete-other-windows)
  ;; Refresh dashboard buffer
  (if (get-buffer dashboard-buffer-name)
      (kill-buffer dashboard-buffer-name))
  (dashboard-insert-startupify-lists)
  (switch-to-buffer dashboard-buffer-name))

;(setq org-ditaa-jar-path "/usr/bin/ditaa")

(use-package pdf-tools
  :config
  (pdf-tools-install)
  (setq-default pdf-view-display-size 'fit-width))

(add-hook 'pdf-view-mode-hook (lambda() (linum-mode -1)))

(use-package org
    :ensure org-contrib
    :config
    (setq org-ellipsis " ▾")

    (setq org-agenda-start-with-log-mode t)
    (setq org-log-done 'time)
    (setq org-log-into-drawer t)

    (setq org-default-notes-file "~/org/notes.org")
    (setq org-directory "~/org/")
    (setq org-auto-tangle-default t)
    (setq org-agenda-files '("~/org/agenda.org"))

    (setq org-hide-emphasis-markers t)

(setq org-todo-keywords
 '((sequence "TODO"
      "NEXT"
      "PROJ"
      "PAUSED"
      "|"
      "DONE"
      "CANCELLED")))
(setq org-todo-keyword-faces
        '(("TODO" . "coral")
          ("NEXT" . "cyan")
          ("PROJ" . "orange")
          ("DONE" . "green")
          ("PAUSED" . "IndianRed1")
          ("CANCELLED" . "grey")))

    ;; Save Org buffers after refiling!
    (advice-add 'org-refile :after 'org-save-all-org-buffers)

    ;; etiquetas que utilizo para mis notas
    (setq org-tag-alist '(("@nota" . ?n)
                          ("@casa" . ?c)
                          ("@finanzas" . ?d)
                          ("@fecha" . ?f)
                          ("@salud" . ?s)
                          ("@tarea" . ?t)
                          ("@coche" . ?h)
                          ("@trabajo" . ?b)
                          ("crypt" . ?C)))
    (setq org-tags-exclude-from-inheritance '("crypt" "project"))

    ;; Progress Logging
    ;; When a TODO item enters DONE, add a CLOSED: property with current date-time stamp and into drawer
    (setq org-log-done 'time)
    (setq org-log-into-drawer "LOGBOOK")
    ;;
    ;; Alinea etiquetas
    (setq org-tags-column 70))


  ;; Aspecto mejorado al identar
  (add-hook 'org-mode-hook 'org-indent-mode)


  ;; Finalmente haremos que cuando se visualice un fichero con extensión .org éste se adapte a la ventana y cuando la línea llegue al final de esta, haga un salto de carro.
  (add-hook 'org-mode-hook 'visual-line-mode)

(require 'org-superstar)
(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))

(setq org-ellipsis "▼")

(setq org-superstar-headline-bullets-list '("◉" "●" "○" "◆" "●" "○" "◆"))             (setq org-superstar-item-bullet-alist '((?+ . ?➤) (?- . ?✦)))

(use-package visual-fill-column
  :ensure t
  :hook (org-mode . visual-fill-column-mode)
  :custom
  (visual-fill-column-center-text t)
  (visual-fill-column-width 100)
  (visual-fill-column-mode 1))

(use-package org-contacts
  :ensure nil
  :after org
  :custom
  (org-contacts-files '("~/org/contacts.org")))

(global-set-key (kbd "<f7>") 'org-agenda)

(use-package org-super-agenda
        :ensure t
        :config
        (org-super-agenda-mode))

   (setq org-agenda-skip-scheduled-if-done t
            org-agenda-skip-deadline-if-done t
            org-agenda-compact-blocks t
            org-agenda-window-setup 'current-window
            org-agenda-start-on-weekday 1
            org-deadline-warning-days 7
            org-agenda-time-grid '((daily today require-timed)))

(require 'appt)
(appt-activate 1)

(use-package notifications
  :demand t)
(add-hook 'org-finalize-agenda-hook 'org-agenda-to-appt) ;; update appt list on agenda view

(setq appt-display-format 'window
      appt-message-warning-time '5)
(setq appt-disp-window-function
      (lambda (nmins curtime msg)
        (notifications-notify :title "Recordatorio!!"
                              :body (format "Tienes una cita %s en %d minutos" msg (string-to-number nmins))
                              :app-name "Emacs: Org"
                              :sound-name "alarm-clock-elapsed")))
(display-time)   ;; activate time display
(run-at-time "24:01" 3600 'org-agenda-to-appt)   ;; update appt list hourly
(setq org-agenda-finalize-hook 'org-agenda-to-appt)

(use-package org-capture
 :ensure nil
 :after org
 :bind ("C-c c" . org-capture)
 :preface
 (defvar my/org-basic-task-template "* PORHACER %?
  Añadido: %U" "Plantilla básica de tareas.")

 (defvar my/org-meeting-template "* Cita con %^{CON}
  :PROPERTIES:
  :SUMMARY: %^{DESCRIPCION ....}
  :NOMOBRE: %^{NOMBRE ....}
  :LUGAR: %^{DONDE ....}
  :DIRECCION: %^{CALLE ....}
  :TELEFONO: %^{123-456-789}
  :NOTA: %^{NOTAS}
  :AÑADIDA: %U
  :END:
  Fecha de la reunión: %?%T" "Plantilla para programar reuniones.")

 (defvar my/org-contacts-template "* %(org-contacts-template-name)
  :PROPERTIES:
  :EMAIL: %(org-contacts-template-email)
  :PHONE: %^{123-456-789}
  :HOUSE: %^{123-456-789}
  :ALIAS: %^{nick}
  :NICKNAME: %^{hefistion}
  :IGNORE:
  :NOTE: %^{NOTA}
  :ADDRESS: %^{Calle Ejemplo 1 2A, 28320, Pinto, Madrid, España}
  :BIRTHDAY: %^{yyyy-mm-dd}
  :END:" "Plantilla para org-contacts.")
 :custom
 (org-capture-templates
  `(("c" "Contactos" entry (file+headline "~/org/contacts.org" "Amigos"),
     my/org-contacts-template)

    ("f" "Fecha" entry (file+headline "~/org/reubicar.org" "Fechas"),
     my/org-basic-task-template)

    ("n" "Nota" entry (file+headline "~/org/reubicar.org" "Nota"),
     my/org-basic-task-template)

    ("i" "Cita" entry (file+datetree "~/org/diario.org"),
     my/org-meeting-template)

    ("b" "Blog" entry (file+headline "~/org/reubicar.org" "Blog"),
     my/org-basic-task-template)

    ("t" "Tarea" entry (file+headline "~/org/reubicar.org" "Tareas"),
     my/org-basic-task-template))))

(use-package org-crypt
  :ensure nil
  :after org)
(org-crypt-use-before-save-magic)
(setq org-tags-exclude-from-inheritance (quote ("crypt")))
(setq org-crypt-key "xxxx") ;;poner aqui nombre usuario
(setq org-crypt-disable-auto-save nil)

(setq org-use-speed-commands 'my-org-use-speed-commands-for-headings-and-lists)
(with-eval-after-load 'org
  (let ((listvar (if (boundp 'org-speed-commands) 'org-speed-commands
                   'org-speed-commands-user)))
    (add-to-list listvar '("A" org-archive-subtree-default))
    (add-to-list listvar '("a" org-todo "ARCHIVAR"))
    (add-to-list listvar '("b" org-todo "BLOQUEADO"))
    (add-to-list listvar '("e" org-todo "ENPROCESO"))
    (add-to-list listvar '("x" org-todo "HECHO"))
    (add-to-list listvar '("x" org-todo-yesterday "HECHO"))
    (add-to-list listvar '("s" call-interactively 'org-schedule))
    (add-to-list listvar '("i" call-interactively 'org-clock-in))
    (add-to-list listvar '("o" call-interactively 'org-clock-out))
    (add-to-list listvar '("d" call-interactively 'org-clock-display))
    (add-to-list listvar '("$" call-interactively 'org-archive-subtree))))

;; Cambiamos el estado de la tarea cuando inicia o para el reloj
(setq org-clock-in-switch-to-state "ENPROCESO")
(setq org-clock-out-switch-to-state "BLOQUEADO")

;    (use-package org-caldav
;      :ensure t
 ;     :bind ([f4] . org-caldav-sync)
  ;    :preface
       ;:custom
      ;; Calendarios a utilizar
     ; (org-caldav-url "https://xxxxx/caldav/carlos/")
      ;(org-caldav-url "https://xxxxxs/caldav/group==familia")
      ;(org-caldav-calendars
     ;  '((:calendar-id "carlos"
      ;                 :files ("~/org/agenda.org")
      ;                 :inbox "~/org/agenda/diario-ibx.org")))
      ;(org-caldav-backup-file "~/.personal/calendario/org-caldav-backup.org.pgp")
      ;(org-caldav-save-directory "~/.personal/calendario/")
      ;(org-icalendar-alarm-time 1)
      ;)

(defun my/mi-calendario ()
    "calendarios a mostrar en calfw-org cuando pulso f8"
    (interactive)
    (cfw:open-calendar-buffer
     :contents-sources
     (list
      (cfw:org-create-source)
      )))


  (defun my-org-use-speed-commands-for-headings-and-lists ()
      "Activate speed commands on list items too."
    (or (and (looking-at org-outline-regexp) (looking-back "^\**" nil))
        (save-excursion (and (looking-at (org-item-re)) (looking-back "^[ \t]*" nil)))))

(setq calendar-month-name-array
      ["Enero" "Febrero" "Marzo" "Abril" "Mayo" "Junio"
       "Julio"    "Agosto"   "Septiembre" "Octubre" "Noviembre" "Diciembre"])

(setq calendar-day-name-array
      ["Domingo" "Lunes" "Martes" "Miércoles" "Jueves" "Viernes" "Sábado"])

(setq org-icalendar-timezone "Europe/Madrid") ;; timezone
(setq calendar-week-start-day 1) ;; la semana empieza el lunes
(setq european-calendar-style t) ;; estilo europeo

(setq calendar-holidays '((holiday-fixed 1 1 "Año Nuevo")
                          (holiday-fixed 1 6 "Reyes Magos")
                          (holiday-fixed 3 19 "San José")
                          (holiday-fixed 5 1 "Dia del Trabajo")
                          (holiday-fixed 5 2 "Comunidad de Madrid")
                          (holiday-fixed 5 15 "San Isidro")
                          (holiday-fixed 8 15 "Asunción")
                          (holiday-fixed 10 12 "Día de la Hispanidad")
                          (holiday-fixed 11 01 "Todos los Santos")
                          (holiday-fixed 11 09 "Almudena")
                          (holiday-fixed 12 06 "Constitución")
                          (holiday-fixed 12 08 "Inmaculada")
                          (holiday-fixed 12 25 "Navidad")
                          ))

(use-package calfw
  :ensure t
  :bind ([f8] . my/mi-calendario)
  :custom
  (cfw:org-overwrite-default-keybinding t)) ;; atajos de teclado de la agenda org-mode
;; (setq cfw:display-calendar-holidays nil) ;; para esconder fiestas calendario emacs

(use-package calfw-org
  :ensure t)

(org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     ))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("d" . "description"))

;; Automatically tangle our Emacs.org config file when we save it
(defun efs/org-babel-tangle-config ()
  (when (string-equal (file-name-directory (buffer-file-name))
                      (expand-file-name user-emacs-directory))
    ;; Dynamic scoping to the rescue
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'efs/org-babel-tangle-config)))

(use-package lsp-mode
  :ensure t
  :defer t
  :commands (lsp lsp-deferred)
  :init (setq lsp-keymap-prefix "C-c l")
  :hook (python-mode . lsp-deferred))

;; Provides visual help in the buffer
  ;; For example definitions on hover.
  ;; The `imenu` lets me browse definitions quickly.
  (use-package lsp-ui
    :ensure t
    :defer t
    :config
    (setq lsp-ui-sideline-enable nil
          lsp-ui-doc-delay 2)
    :hook (lsp-mode . lsp-ui-mode)
    :bind (:map lsp-ui-mode-map
                ("C-c i" . lsp-ui-imenu)))

(use-package lsp-treemacs
  :after lsp
  :custom
  (lsp-treemacs-theme "all-the-icons"))

(use-package lsp-ivy)

(use-package dap-mode
  :ensure t
  :defer t
  :after lsp-mode
  :config
  (dap-auto-configure-mode))

(use-package lsp-pyright
    :ensure t
    :defer t
    :config
    (setq lsp-pyright-disable-language-service nil
          lsp-pyright-disable-organize-imports nil
          lsp-pyright-auto-import-completions t
          lsp-pyright-use-library-code-for-types t
          lsp-pyright-venv-path "~/.virtualenvs/")
    :hook ((python-mode . (lambda ()
                            (require 'lsp-pyright) (lsp-deferred)))))

  ;; Built-in Python utilities (necesarios)
  (use-package python
    :ensure t
    :config
    ;; Remove guess indent python message
    (setq python-indent-guess-indent-offset-verbose nil))

  ;; Hide the modeline for inferior python processes
  (use-package inferior-python-mode
    :ensure nil
    :hook (inferior-python-mode . hide-mode-line-mode))

  ;; Required to hide the modeline
  (use-package hide-mode-line
    :ensure t
    :defer t)

  ;; Format the python buffer following YAPF rules
  ;; There's also blacken if you like it better.
  (use-package yapfify
    :ensure t
    :defer t
    :hook (python-mode . yapf-mode))

;; flycheck por flymake
  (use-package flycheck
    :ensure t
    :init (global-flycheck-mode))

(use-package virtualenvwrapper
  :ensure t
  :config
  (venv-initialize-interactive-shells)
  (venv-initialize-eshell))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "markdown_py"))

;; Provides completion, with the proper backend
;; it will provide Python completion.

(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
              ("<tab>" . company-complete-selection))
  (:map lsp-mode-map
        ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :ensure
  :bind
  ("C-x g" . magit-status))

(use-package git-gutter
  :ensure t
  :defer 0.3
  :delight
  :init (global-git-gutter-mode))

(use-package git-timemachine
  :defer 1
  :delight)

;; Mostrar al padre
(show-paren-mode 1)

;; auto close bracket insertion.
(electric-pair-mode 1)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package ox-hugo
  :ensure t
  :after ox)

(use-package eshell-git-prompt)

(use-package eshell
  :config
  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)
    (setq eshell-visual-commands '("htop" "zsh" "vim"))
    (setq eshell-history-size         10000
          eshell-buffer-maximum-lines 10000
          eshell-hist-ignoredups t
          eshell-scroll-to-bottom-on-input t))
  (eshell-git-prompt-use-theme 'robbyrussell))

(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first")))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

;; Por defecto los fiocheros ocultos no se muestran hasta que pulsamos .
(use-package dired-hide-dotfiles
  :ensure t
  :bind
  (:map dired-mode-map
        ("." . dired-hide-dotfiles-mode))
  :hook
  (dired-mode . dired-hide-dotfiles-mode))

;; Provides workspaces with file browsing (tree file viewer)
 ;; and project management when coupled with `projectile`.

 (use-package treemacs
   :ensure t
   :defer t
   :init
   (with-eval-after-load 'winum
     (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
   :config
   (progn
     (setq treemacs-show-cursor                   t
           treemacs-user-mode-line-format         t
           treemacs-width                         24)
     (treemacs-follow-mode t)
     (treemacs-filewatch-mode t)
     (treemacs-fringe-indicator-mode 'always))
   :bind
   (:map global-map
         ("M-0"       . treemacs-select-window)
         ("C-x t t"   . treemacs)
         ("C-x t B"   . treemacs-bookmark)
         ("C-x t C-t" . treemacs-find-file)
         ("C-x t M-t" . treemacs-find-tag)))

 (use-package treemacs-projectile
   :after (treemacs projectile)
   :ensure t)

 (use-package treemacs-all-the-icons
   :after treemacs
   :config
   (treemacs-load-theme "all-the-icons"))

   (use-package treemacs-magit
     :after (treemacs magit)
     :ensure t)

(use-package treemacs-perspective ;;treemacs-perspective if you use perspective.el vs. persp-mode
  :after (treemacs perspective-mode) ;;or perspective vs. persp-mode
  :ensure t
  :config (treemacs-set-scope-type 'Perspectives))

(setq mastodon-instance-url "https://im-in.space"
     mastodon-active-user "@mester@im-in.space")

(use-package telega
  :commands telega
  :defer t
  :custom
  (telega-use-tracking-for '(any pin unread))
  (telega-chat-use-markdown-formatting t)
  (telega-emoji-use-images t)
  (telega-completing-read-function #'selectrum-completing-read)
  (telega-msg-rainbow-title t)
  (telega-chat-fill-column 75)
  (telega-appindicator-mode t)
  (telega-notifications-mode t)
  (telega-filters-custom nil))

(define-key global-map (kbd "C-c t") telega-prefix-map)
(setq telega-server-libs-prefix "/usr")

(use-package tex
:ensure auctex
:hook (LaTeX-mode . reftex-mode)
:custom
(TeX-PDF-mode t)
(TeX-auto-save t)
(TeX-byte-compile t)
(TeX-clean-confirm nil)
(TeX-master 'dwim)
(TeX-parse-self t)
(TeX-source-correlate-mode t)
(TeX-view-program-selection '((output-pdf "Okular")
			      (output-html "xdg-open"))))

(use-package bibtex
:after auctex
:hook (bibtex-mode . my/bibtex-fill-column)
:preface
(defun my/bibtex-fill-column ()
  "Ensures that each entry does not exceed 120 characters."
  (setq fill-column 120)))

(use-package company-auctex
  :after (auctex company)
  :config (company-auctex-init))

(use-package company-math :after (auctex company))

(setq-default TeX-engine 'xetex)
(use-package reftex :after auctex)
(use-package smart-mode-line
     :defer 0.1
     :custom (sml/theme 'respectful)
     :config (sml/setup))
(use-package all-the-icons :ensure t)
