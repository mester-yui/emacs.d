# Configuracion de emacs

Esta es mi configuracion de emacs

La configuracion es exportada a el archivo [init.el](init.el) desde el archivo [config.org](config.org)

Esta configuracion necesita

_Texlive core_ y _Texlive-Latex-Extra_ junto con _Auctex_ y _libtd_
